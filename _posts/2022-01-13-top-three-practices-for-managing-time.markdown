---
layout: post
title:  Top three practises for managing your time with flexible work
date:   2022-01-13 10:15:19 +0100
categories: productivity
read_time: true

# optional alternate title to replace page.title at the top of the page
alt_title: "Top three practises for managing your time with flexible work"

# optional sub-title below the page title
sub_title: 

# optional intro text below titles, Markdown allowed
introduction:     

# optional call to action links
actions:

image: 

---

Before Covid I was rarely working from home. Our company culture at [Ocado Technology](https://www.ocadogroup.com/technology/technology-pioneers) was very office-centric, benefiting from the presence and relationships of our technologists. Having everybody around equalled fast problem resolution or being able to practice my favourite management style, management by wandering around. Building face to face relationships was (and still is) incredibly important and it remains a huge part of our DNA at [Ocado Technology](https://www.ocadogroup.com/careers/technology) — this is how we collaborate, learn and innovate. However, the situation has changed, and whether or not we wanted to start working remotely, we had to. To help people cope with this drastic change, Ocado Technology decided to relax the concept of core business hours, where everybody was expected to be available. As a result, **a new concept was introduced, closely resembling *flexible work***. This freedom allows us to suit individual business areas, functions and roles, and their needs. However, old habits die hard, and over the first couple of months it was business as usual, with the only difference to the pre-covid times of kids, pets and spouses walking in the background of video calls.

> You need to be much more deliberate in managing your time to really benefit from flexible work

Quickly it turned out that this *temporary* state may not end anytime soon, leading to experiments with different practises and recommendations conducted by plenty of individuals and teams. We recognise that one size doesn’t fit all, so each business area leads the approach to the way they work. Same goes for me, and trying out a bit of different time management practises I finally decided to implement a couple of changes to my habits. Here you can read **my top three recommendations for making the best use of flexible work in your life**.

![map-image](/assets/images/doors.jpg)

### Protect focus time

Starting with one that I believe is the most popular practice out there. It is so popular for a reason. Protecting the weekly bare minimum for deep work, is not only good for your productivity, but also for your wellbeing. For me personally, the two hours block is the minimum to focus and do something meaningful, but the more the better generally. **For my own peace of mind I’m trying to squeeze at least two Focus Time blocks weekly.**

### Play to your strengths

I can’t stress this enough how impactful learning your own productivity type can be. For example, consider the scale from early bird to night owl. Personally, I’m much closer to the night owl, reaching my productivity peak at around 4 PM. To make better use of my time, I moved my working hours a bit, making myself available for meetings between 10AM and 6PM, with the exception of lunchtime which I’m spending with my family. This enabled me to have a good night sleep every day, waking up naturally (yes, no alarm clock!). As I began working a bit more late in the afternoon, I started scheduling my messages to go out in the morning the next day. Otherwise people I work with could read this subconsciously as an expectation to work late. This is a misunderstanding worth avoiding of course, as it would have a negative impact, especially if you are an early bird.

### Block the personal time

To my surprise, one of the most impactful changes was a really trivial one. I started synchronising the private calendar with the work calendar, copying the events. Whenever I have something private planned, my work calendar shows either “busy” or “personal commitment”. I’m not super-strict keeping these slots, sometimes joining the calls from my car or rescheduling less important personal commitments. Nevertheless, it benefited my personal life a lot, giving me enough time to catch up on doctor visits, improving mental and physical health and handling a lot of overdue responsibilities avoiding the queues in offices or high traffic in the afternoon or early morning. It’s also worth pointing out that booking personal time in the work calendar works as an example that it’s totally fine to take care of your personal life during the day with current work arrangements.

## Wrapping up

These simple practises really unlocked the *flexibility* in flexible work for me. After almost two years working this way, my biggest finding about flexible work is that it’s not flexible at all when it comes to managing your calendar. **You need to be much more deliberate in managing your time.** There are very few products that help you automate time management ([Reclaim](https://reclaim.ai/), [Clockwise](https://www.getclockwise.com/)), but still in a rather early phase of development and with ridiculously high prices. Still, you can get a lot of value by making some adjustments yourself, and I truly recommend trying out my top 3 practises to find yourself in an ever-evolving future.

Interested in productivity in IT? Observe me on [twitter](https://twitter.com/mariuszluciow) or [medium](https://mariuszluciow.medium.com/) for any future articles on productivity myths or good and bad practices in this space.

*Special thank you to [Aleksandra Stasiak](https://www.linkedin.com/in/aleksandra-stasiak92) for helping with review.*

