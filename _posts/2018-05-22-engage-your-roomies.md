---
layout: post
title:  "Engage your roomies - Home Automation"
date:   2018-05-22 12:15:19 +0100
categories: automation recipies
read_time: true

# optional alternate title to replace page.title at the top of the page
alt_title: "Overcome the Home Automation biggest difficulty"

# optional sub-title below the page title
sub_title: "Engage your roomies!"

# optional intro text below titles, Markdown allowed
introduction: |
    My strategy to get my wife's attention is to prepare a dessert. I found one which is especially 
    convincing - I like to call it Memory Lane Panna Cotta. Originally, Panna Cotta is an Italian dessert 
    of sweetened cream thickened with gelatin and molded. Usually it is flavoured with vanillia. 

---

Right now, with the amount of devices and integrations available it is relatively simple to 
have a smart home. The real challenge is to convince your loved ones, you share your home with,
to start using this new automations. My weapon of choice in this war is my special Memory Lane Panna Cotta.

![map-image](/assets/images/pana.jpg)

### Why memory lane

I believe, that there is a secret ingridient that makes a good dessert really great one. It's not love or hunger as you might guess - the secret to mind blowing
treat is the taste memory. We all have taste remembrances from our childhood. 
These memories are the reason why no one cooks better than our moms and grandmas. We all are craving for the taste we like so much when we were kids. 

My dessert is build with taste memory in mind. The main ingridient of plain panna cotta is milk. I remember eating
tons of cereals as a kid (my favourite Cookie Crisp, omnomnomn!). After devauring the cereals, the thing that was left
in the bowl was milk, having beautiful taste of Cookie Crtisps. This cereal lefover is the flavour carrier of my panna cotta.  

## Ingredients

Panna Cotta, even with this flavour twist is very simple dessert to make. You will need following ingridients:

*(4 - 5 portions)*

* 3 teaspoons of granulated gelatin
* 200 ml of heavy cream (30% or 36%)
* 500 ml of milk
* 30 g of sugar
* bowl of your favourite cereals (if these are not sweet you can add more sugar, up to 60 g)

To decorate:

* strawberries (can be frozen) or raspberries to make a fruit mousse
* grated chocolate or nuts
* mint leafs

## The recipe 

First we need to create our cereal-flavoured milk. In a small pot mix milk with cereals, heat a little (don't let it boil)
and leave it to cool down. In the meantime, mix your gelatine with 2 tablespoons of cold water.
After 5 minutes or so drain the milk from cereals, possibly using dense strainer. 
You should have exactly 300 ml of milk (add some fresh if cereals drained too much, or pour some out if you have too much).

Pour milk and heavy cream into small bowl, add the sugar. Heat till sugar dissolves, constantly mixing with a kitchen whisk. 
Right before it boils stop heating, add the gelatine and continue mixing with whisk for at least minute. Leave it to 
cool down a little bit, then pour into small glasses. Cover them with something (tin foil for example) and put into the fridge to set, 
for at least 5 hours (best all night).

## Serving

You can either serve it strait from the glass, but it looks really nice on a small plate. To take it out from the glass, you can put it 
to hot water for few seconds to melt down a little around the edges. To decorate your dessert prepare a strawberry mousse (just mix some strawberries)
and pour it on top. You can also sprinkle a litte granted chocolate or nuts and add a mint leaf.

## Summary

As you can see, the Memory Lande Panna Cotta is extremally simple to make, but don't let it fool you - it's one of the 
best desserts you can make, assuming of course that you like cereals. Happy Cooking!
