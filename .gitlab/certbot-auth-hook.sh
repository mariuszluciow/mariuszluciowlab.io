#!/bin/bash

echo $CERTBOT_VALIDATION > .well-known/acme-challenge/$CERTBOT_TOKEN
git add .
git commit -m "Adding acme-challenge file"
git push origin master

echo "Sleeping 30 minutes to give gitlab time to reload..."
sleep 1800