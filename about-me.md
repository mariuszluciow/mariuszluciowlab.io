---
layout: about
title: About Me
permalink: /about-me/
---

Mariusz Łuciów - Head of Engineering / Principal Software Developer
Leading Development Services at [Ocado Technology](https://www.ocadogroup.com/technology/technology-pioneers).
❤ Engineering Productivity 

Inspiring leader proven in many projects. Years of experience in design and development of enterprise applications. Enthusiast of applying software architecture patterns into everyday challenges. Last couple of years deeply involved in providing internal tools and platforms, strongly focused on cloud adoption.

This blog contains things I consider worth remembering, tutorials and articles about variety of topics including Java, 
Home Automation, Infrastructure, Cloud and Front-End. 
